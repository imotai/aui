(function($) {
    /*
        tabbable(), visible() and focusable() functions from jQuery UI v 1.10.43
        Code modified slightly to be compatible with jQuery < 1.8 (.addBack() replaced with .andSelf()),
        and refactored to aid readability and conform to AUI coding standards
    */
    function visible( element ) {
        return !$( element ).parents().andSelf().filter(function() {
            return $.curCSS( this, "visibility" ) === "hidden" ||
                $.expr.filters.hidden( this );
        }).length;
    }

    function focusable( element, isTabIndexNotNaN ) {
        var nodeName = element.nodeName.toLowerCase();

        if (nodeName === 'area') {
            var map = element.parentNode;
            var mapName = map.name;
            var imageMap = $('img[usemap=#' + mapName + ']').get();

            if (!element.href || !mapName || map.nodeName.toLowerCase() !== 'map') {
                return false;
            }
            return imageMap && visible(imageMap);
        }
        var isFormElement = /input|select|textarea|button|object/.test(nodeName);
        var isAnchor = nodeName === 'a';
        var isAnchorTabbable = (element.href || isTabIndexNotNaN);

        return (
            isFormElement ? !element.disabled :
            (isAnchor ? isAnchorTabbable : isTabIndexNotNaN)
        ) && visible(element);
    }

    function tabbable( element ){
        var tabIndex = $.attr( element, "tabindex" ),
            isTabIndexNaN = isNaN( tabIndex );
        var hasTabIndex = ( isTabIndexNaN || tabIndex >= 0 );

        return hasTabIndex && focusable( element, !isTabIndexNaN );
    }

    function getTabbable($dialog){
        return $dialog.find('*').filter(function (index) {
            return tabbable(this);
        });
    }

    //Bind an event to wrap around the dialog's tabbable elements
    (function(){
        $(document).on('focusout', '.aui-dialog2',  function(event) {
            var $topMostDialog = AJS.LayerManager.global.getTopLayer();
            if (!$topMostDialog) {
                return;
            }

            var focusFrom = event.target;
            var focusTo = event.relatedTarget;

            AJS.dim.$dim.attr('tabIndex', 0); //required, or the last element's focusout event will go to the browser

            if ($topMostDialog.has(focusTo).length > 0) {
                return;
            }

            var $tabbableElements = getTabbable($topMostDialog);
            var $firstTabbableElement = AJS.$($tabbableElements.first());
            var $lastTabbableElement = AJS.$($tabbableElements.last());

            if ($firstTabbableElement.is(focusFrom)) {
                $lastTabbableElement.focus();
            } else if ($lastTabbableElement.is(focusFrom)) {
                $firstTabbableElement.focus();
            }
        });
    })();

    function setNewFocusElement(dialog){
        var firstTabbableElement = getTabbable(dialog.$el.find('.aui-dialog2-content, .aui-dialog2-footer'))[0] || getTabbable(dialog.$el.find('.aui-dialog2-header'))[0];
        if (firstTabbableElement) {
            firstTabbableElement.focus();
        }
    }

    function saveOldFocusElement(dialog){
        dialog._$oldFocusElement = $(document.activeElement);
    }

    function restoreOldFocusElement(dialog){
        if (dialog._$oldFocusElement) {
            dialog._$oldFocusElement.focus();
        }
    }

    var defaults = {
        "aui-focus-selector": ".aui-dialog2-content :input:visible:enabled",
        "aui-blanketed": "true"
    };

    function applyDefaults($el) {
        jQuery.each(defaults, function(key, value) {
            var dataKey = "data-" + key;
            if (!$el[0].hasAttribute(dataKey)) {
                $el.attr(dataKey, value);
            }
        });
    }

    function Dialog2(selector) {
        if (selector) {
            this.$el = $(selector);
        }
        else {
            this.$el = $(AJS.parseHtml($(aui.dialog.dialog2({}))));
        }
        applyDefaults(this.$el);
    }

    Dialog2.prototype.on = function(event, fn) {
        AJS.layer(this.$el).on(event, fn);
        return this;
    };

    Dialog2.prototype.off = function(event, fn) {
        AJS.layer(this.$el).off(event, fn);
        return this;
    };

    Dialog2.prototype.show = function() {
        saveOldFocusElement(this);

        var layer = AJS.layer(this.$el);
        layer.show();

        setNewFocusElement(this);

        return this;
    };

    Dialog2.prototype.hide = function() {
        AJS.layer(this.$el).hide();
        return this;
    };

    Dialog2.prototype.remove = function() {
        AJS.layer(this.$el).remove();
        return this;
    };

    AJS.dialog2 = AJS._internal.widget('dialog2', Dialog2);

    var GLOBAL_EVENT_PREFIX = "_aui-internal-dialog2-global-";

    function bindToLayerEvent(eventName) {
        AJS.layer.on(eventName, function(e, $el) {
            if ($el.is('.aui-dialog2')) {
                var dialogEvent = AJS.$.Event(GLOBAL_EVENT_PREFIX + eventName);
                $(document).trigger(dialogEvent, [$el]);
                return !dialogEvent.isDefaultPrevented();
            }
            else {
                return true;
            }
        });
    }
    // wrap layer events
    var LAYER_EVENTS = ['show', 'hide', 'beforeShow', 'beforeHide'];
    for (var i = 0; i < LAYER_EVENTS.length; ++i) {
        bindToLayerEvent(LAYER_EVENTS[i]);
    }

    AJS.dialog2.on = function(eventName, fn) {
        $(document).on(GLOBAL_EVENT_PREFIX + eventName, fn);
        return this;
    };

    AJS.dialog2.off = function(eventName, fn) {
        $(document).off(GLOBAL_EVENT_PREFIX + eventName, fn);
        return this;
    };

    /* Live events */

    $(document).on('click', '.aui-dialog2-header-close', function(e) {
        e.preventDefault();
        AJS.dialog2($(this).closest('.aui-dialog2')).hide();
    });

    AJS.dialog2.on('hide', function(e,$el) {
        var layer = AJS.layer($el);

        var dialog = AJS.dialog2($el);
        restoreOldFocusElement(dialog);

        if ($el.data("aui-remove-on-hide")) {
            layer.remove();
        }
    });

})(AJS.$);