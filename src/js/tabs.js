(function (){

    'use strict';

    var REGEX = /#.*/,
        ACTIVE_TAB = "active-tab",
        ACTIVE_PANE = "active-pane",
        ARIA_SELECTED = "aria-selected",
        ARIA_HIDDEN = "aria-hidden",
        DATA_TABS_PERSIST = "data-aui-persist",
        STORAGE_PREFIX = "_internal-aui-tabs-";

    function enhanceTabLink() {
        var thisLink = AJS.$(this);
        AJS._addID(thisLink); // ensure there's an id for later
        thisLink.attr("role","tab");
        var targetPane = thisLink.attr("href"); // remember href includes # for selector
        AJS.$(targetPane).attr("aria-labelledby", thisLink.attr("id"));

        if (thisLink.parent().hasClass(ACTIVE_TAB)) {
            thisLink.attr(ARIA_SELECTED,"true");
        } else {
            thisLink.attr(ARIA_SELECTED,"false");
        }
    }

    function switchToTab($tab) {
        var $pane = AJS.$($tab.attr("href").match(REGEX)[0]);
        $pane.addClass(ACTIVE_PANE).attr(ARIA_HIDDEN,"false")
            .siblings(".tabs-pane").removeClass(ACTIVE_PANE).attr(ARIA_HIDDEN,"true");
        $tab.parent("li.menu-item").addClass(ACTIVE_TAB)
            .siblings(".menu-item").removeClass(ACTIVE_TAB);
        $tab.closest(".tabs-menu").find("a").attr(ARIA_SELECTED,"false");
        $tab.attr(ARIA_SELECTED,"true");
        $tab.trigger("tabSelect", {
            tab: $tab,
            pane: $pane
        });
    }

    function isPersistentTabGroup($tabGroup) {
        // Tab group persistent attribute exists and is not false
        return $tabGroup.attr(DATA_TABS_PERSIST) !== undefined && $tabGroup.attr(DATA_TABS_PERSIST) !== "false";
    }

    function createPersistentKey($tabGroup) {
        var tabGroupId = $tabGroup.attr("id");
        var value = $tabGroup.attr(DATA_TABS_PERSIST);

        return STORAGE_PREFIX + (tabGroupId ? tabGroupId : "") + (value && value !== "true" ? "-" + value : "");
    }

    function updateTabsFromLocalStorage($tabGroups) {
        for (var i=0, ii = $tabGroups.length; i < ii; i++) {
            var $tabGroup = $tabGroups.eq(i);
            if (isPersistentTabGroup($tabGroup)) {
                var tabGroupId = $tabGroup.attr("id");
                if (tabGroupId) {
                    var persistentTabId = window.localStorage.getItem(createPersistentKey($tabGroup));
                    if (persistentTabId) {
                        var $tabmatch = $tabGroup.find("#" + persistentTabId);

                        if ($tabmatch.length) {
                            switchToTab($tabmatch);
                        }
                    }
                } else {
                    AJS.warn("A tab group must specify an id attribute if it specifies data-aui-persist");
                }
            }
        }
    }

    function updateLocalStorageEntry($tab) {
        var $tabGroup = $tab.closest(".aui-tabs");

        var tabGroupId = $tabGroup.attr("id");
        if (tabGroupId){
            var tabId = $tab.attr("id");
            if (tabId) {
                window.localStorage.setItem(createPersistentKey($tabGroup),tabId);
            }
        } else {
            AJS.warn("A tab group must specify an id attribute if it specifies data-aui-persist");
        }
    }

    function handleTabClick(e) {
        AJS.tabs.change(AJS.$(this), e);
        e && e.preventDefault();
    }

    AJS.tabs = {
        setup: function () {
            var $tabs = AJS.$(".aui-tabs:not(.aui-tabs-disabled)");

            // Non-menu ARIA setup
            $tabs.attr("role","application");
            $tabs.find(".tabs-pane").each( function() {
                var thisPane = AJS.$(this);
                thisPane.attr("role","tabpanel");
                if (thisPane.hasClass(ACTIVE_PANE)) {
                    thisPane.attr(ARIA_HIDDEN,"false");
                } else {
                    thisPane.attr(ARIA_HIDDEN,"true");
                }
            });

            // Menu setup
            for (var i=0, ii = $tabs.length; i < ii; i++) {
                var $tab = $tabs.eq(i);
                if (!$tab.data("aui-tab-events-bound")) {
                    var $tabMenu = $tab.children("ul.tabs-menu");

                    // ARIA setup
                    $tabMenu.attr("role","tablist");
                    $tabMenu.children("li").attr("role","presentation"); // ignore the LIs so tab count is announced correctly
                    $tabMenu.find("> .menu-item a").each(enhanceTabLink);

                    // Set up click event for tabs
                    $tabMenu.delegate("a", "click", handleTabClick);
                    $tab.data("aui-tab-events-bound", true);
                }
            }

            // Check for persistent tabs
            if (window.localStorage) {
                updateTabsFromLocalStorage($tabs);
            }

            // Vertical tab truncation setup (adds title if clipped)
            AJS.$(".aui-tabs.vertical-tabs").find("a").each(function() {
                var thisTab = AJS.$(this);
                // don't override existing titles
                if ( !thisTab.attr("title") ) {
                    var strong = thisTab.children("strong:first");
                    // if text has been truncated, add title
                    if ( AJS.isClipped(strong) ) {
                        thisTab.attr("title", thisTab.text());
                    }
                }
            });
        },
        change: function ($a, e) {
            switchToTab($a);

            var $tabGroup = $a.closest(".aui-tabs");
            if (window.localStorage && isPersistentTabGroup($tabGroup)) {
                updateLocalStorageEntry($a);
            }
        }
    };
    AJS.$(AJS.tabs.setup);
})();
