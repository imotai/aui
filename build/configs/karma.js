module.exports = {
    options: require('./karma-shared'),
    cli: {
        options: {
            singleRun: true,
            junitReporter: {
                outputFile: 'tests/karma-jquery-<%= jquery %>.xml'
            }
        }
    },
    flatpack: {
        options: {
            junitReporter: {
                outputFile: 'tests/karma-jquery-<%= jquery %>-flatpack.xml'
            },
            frameworks: ['qunit', 'requirejs', 'sinon'],
            singleRun: true,
            files: [
                'src/css/layer.css',
                'src/css/tabs.css',
                'tests/unit/atlassian-js/atlassian-js-test.css',
                '<%= paths.bowerSource %>/jquery/jquery.js',
                {
                    pattern: '<%= paths.dist %>' + 'aui-next/js/*.js',
                    included: false
                },
                // progress-data-set and restful table need to be included manually because the flatpack does not have it
                {
                    pattern: '<%= paths.jsSource %>' + 'experimental-autocomplete/*.js',
                    included: false
                }, {
                    pattern: '<%= paths.jsSource %>' + 'experimental-restfultable/*.js',
                    included: false
                }, {
                    pattern: '<%= paths.jsSource %>' + 'experimental-events/events.js',
                    included: false
                }, {
                    pattern: '<%= paths.jsVendorSource %>' + 'backbone/backbone.js',
                    included: false
                }, {
                    pattern: '<%= paths.jsVendorSource %>' + 'underscorejs/underscore.js',
                    included: false
                }, {
                    pattern: '<%= paths.jsVendorSource %>' + 'jquery/serializetoobject.js',
                    included: false
                },
                // end progress-data-set and restful table

                // tests
                {
                    pattern: 'tests/unit/aui-qunit.js',
                    included: false
                }, {
                    pattern: 'tests/unit/{*,**/*}-test.js',
                    included: false
                },
                'tests/unit/karma-flatpack-main.js'
            ]
        }

    }
};
